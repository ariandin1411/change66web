<!DOCTYPE html>
<html lang="en">
<head>

  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  <meta property="og:locale" content="nb_NO" />

  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="keywords"    content="">
  <meta name="description" content="">
  <meta name="author" content="">
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <link rel="shortcut icon" type="image/png" href="{{ URL::asset('assets/img/iconchange66.png') }}" />

  <title>@yield('title')</title>

  <!-- Bootstrap core CSS -->
  {{-- <link href="{{ URL::asset('assets/vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet"> --}}
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">


  <!-- Custom fonts for this template -->
  <link href="{{ URL::asset('assets/vendor/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic" rel="stylesheet" type="text/css">
  <link href='https://fonts.googleapis.com/css?family=Cabin:700' rel='stylesheet' type='text/css'>


  <!-- Custom styles for this template -->
  <link href="{{ URL::asset('assets/css/grayscale.min.css') }}" rel="stylesheet">
  <link href="{{ URL::asset('assets/css/slidequote.css') }}" rel="stylesheet">
  <link href="{{ URL::asset('assets/css/content.css') }}" rel="stylesheet">
  <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">

  @yield('addingStyle')
  <link href="{{ URL::asset('css/style-ari-front.css') }}" rel="stylesheet">

  <!-- <link href="css/bootstrap-4-hover-navbar.css" rel="stylesheet"> -->

</head>
<a href="/internett1/index.php?t=publish.index&inline=edit" accesskey="E"></a>
{{-- <style type="text/css"> .container{ word-break: normal; }</style> --}}
<body id="page-top">

{{-- navbar menu new --}}
  <nav class="menuatas navbar navbar-default" style="background-color: #3a3838;border-color: #3a3838;">
    <div class="container">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar" style="background-color: white;"></span>
          <span class="icon-bar" style="background-color: white;"></span>
          <span class="icon-bar" style="background-color: white;"></span>
        </button>
         <a class="" href="/">
          <img src="{{ URL::asset('assets/img/logochange66.png') }}" style="height:53px; width:193px" alt="" />
        </a>
      </div>

      <!-- Collect the nav links, forms, and other content for toggling -->
      <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
         <ul class="nav navbar-nav" style="text-transform: uppercase;font-family: Cabin,'Helvetica Neue',Helvetica,Arial,sans-serif;"><!-- mx-auto -->
          @foreach( Menus::getNavbar(['NodeID' => Menus::getLangNode()]) as $menu )

            @if( count( Menus::getNavbar(['NodeID' => $menu->id]) ) > 0 )

              <li class="dropdown">
                @if($menu->id == 32)
                  <a href="{{ $menu->alias }}" class="nav-link js-scroll-trigger"> {{ $menu->title }}
                    <b class="caret"></b>
                  </a>
                @else
                  <a href="/{{ $menu->alias }}" class="nav-link js-scroll-trigger"> {{ $menu->title }}
                    <b class="caret"></b>
                  </a>
                @endif
                  <ul class="dropdown-menu" style="background-color: #909090;border: 0px solid rgba(0,0,0,.15);margin-top: 24px;">
                    @foreach( Menus::getNavbar(['NodeID' => $menu->id, 'whereNotIn' => [30]]) as $menusub )
                      <li><a href="/{{ $menusub->alias }}" class="nav-link">{{ $menusub->title }}</a></li>
                    @endforeach
                  </ul>
              </li>

            @else

              <li>
                <a href="/{{ $menu->alias }}" class="nav-link js-scroll-trigger">{{ $menu->title }}</a>
              </li>

            @endif

          @endforeach
          <li><a href="{{ route('registerfronts') }}" class="nav-link js-scroll-trigger">{{ Menus::getLanguageString('idRegister3') }}</a></li>
          
        </ul>
        <div class="my-2 my-lg-0" style="padding:12px;display: inline-block;">
          <a href="{{ route('changeLanguage', ['langID' => 'no']) }}">
            <img src="{{ URL::asset('assets/img/norwegia.png') }}" style="height:18px; width:23px;" alt="" />
          </a>
          <a href="{{ route('changeLanguage', ['langID' => 'en']) }}">
            <img src="{{ URL::asset('assets/img/flag-US.png') }}" style="height:18px; width:23px;" alt="" />
          </a>
         </div>
      </div><!-- /.navbar-collapse -->

    </div>
  </nav>
{{-- /navbar menu new --}}

  <!-- Navigation -->
  {{-- <nav class="menuatas navbar navbar-expand-lg navbar-light fixed-top @yield('addingNavBar')" id="mainNav">
    <div class="container-fluid col-md-9 garis-9">
      <a class="navbar-brand js-scroll-trigger" href="/">
        <img src="{{ URL::asset('assets/img/logowise.png') }}" style="height:53px; width:193px" alt="" />
      </a>
      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        Menu
        <i class="fa fa-bars"></i>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">

        <ul class="navbar-nav"><!-- mx-auto -->
          @foreach( Menus::getNavbar(['NodeID' => Menus::getLangNode()]) as $menu )

            @if( count( Menus::getNavbar(['NodeID' => $menu->id]) ) > 0 )

              <li class="dropdown">
                @if($menu->id == 32)
                  <a href="{{ $menu->alias }}" class="nav-link js-scroll-trigger"> {{ $menu->title }}
                    <b class="caret"></b>
                  </a>
                @else
                  <a href="/{{ $menu->alias }}" class="nav-link js-scroll-trigger"> {{ $menu->title }}
                    <b class="caret"></b>
                  </a>
                @endif
                  <ul class="dropdown-menu" style="background-color: #3180a9;border: 0px solid rgba(0,0,0,.15);">
                    @foreach( Menus::getNavbar(['NodeID' => $menu->id, 'whereNotIn' => [30]]) as $menusub )
                      <li><a href="/{{ $menusub->alias }}" class="nav-link">{{ $menusub->title }}</a></li>
                    @endforeach
                  </ul>
              </li>

            @else

              <li>
                <a href="/{{ $menu->alias }}" class="nav-link js-scroll-trigger">{{ $menu->title }}</a>
              </li>

            @endif

          @endforeach
          @guest
              <li>
                <a href="{{-- route('loginfront') --}}" class="nav-link js-scroll-trigger">
                  {{ Menus::getLanguageString('idLogin') }}
                </a>
              </li> --}}

              {{-- <li><a href="{{ route('register') }}" class="nav-link js-scroll-trigger">Register</a></li> --}}
          {{-- @else
          <li>
            <a href="/{{ session()->get('LanguageID') }}/profiles" class="nav-link js-scroll-trigger">
              {{ Menus::getLanguageString('idMyProfile') }}
            </a>
          </li>
          <li>
              <a href="{{ route('logout') }}"
                  onclick="event.preventDefault();
                           document.getElementById('logout-form').submit();" class="nav-link js-scroll-trigger">
                  {{ Menus::getLanguageString('idLogOut') }}
              </a>

              <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                  {{ csrf_field() }}
              </form>
          </li>
          @endguest
        </ul>
        <div class="my-2 my-lg-0" style="padding:12px;display: inline-block;">
          <a href="{{ route('changeLanguage', ['langID' => 'no']) }}">
            <img src="{{ URL::asset('assets/img/norwegia.png') }}" style="height:18px; width:23px;" alt="" />
          </a>
          <a href="{{ route('changeLanguage', ['langID' => 'en']) }}">
            <img src="{{ URL::asset('assets/img/flag-US.png') }}" style="height:18px; width:23px;" alt="" />
          </a>
         </div>
       </div>
  </div>
</nav> --}}

<!-- Intro Header -->

@yield('content')





        <!-- Footer -->
        <footer style="background-color: #3a3838;">
          <div class="container text-center hilang">
            <p>Copyright &copy; WiseHouse 2018</p>
            @if (session()->get('LanguageID') == 'no')
              <p><a href="{{ url('/no/Personvernerklæring') }}">{{ Menus::getLanguageString('idTermsOfUse') }}</a></p>
              <ul class="list-unstyled">
                @foreach( Menus::getNavbar(['NodeID' => 48]) as $menusub )
                  <li><a href="/{{ $menusub->alias }}" class="nav-link">{{ $menusub->title }}</a></li>
                @endforeach
              </ul>
            @elseif (session()->get('LanguageID') == 'en')
              <p><a href="{{ url('en/Privacy_Statement') }}">{{ Menus::getLanguageString('idTermsOfUse') }}</a></p>
              <ul class="list-unstyled">
                @foreach( Menus::getNavbar(['NodeID' => 59]) as $menusub )
                  <li><a href="/{{ $menusub->alias }}" class="nav-link">{{ $menusub->title }}</a></li>
                @endforeach
              </ul>
            @endif

           
          </div>
        </footer>

        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-1857730-54"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'UA-1857730-54');
        </script>

        <!-- Bootstrap core JavaScript -->
        <script src="{{ URL::asset('assets/vendor/jquery/jquery.min.js') }}"></script>
        {{-- <script src="{{ URL::asset('assets/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script> --}}
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

        <!-- Plugin JavaScript -->
        <script src="{{ URL::asset('assets/vendor/jquery-easing/jquery.easing.min.js') }}"></script>

        <!-- Custom scripts for this template -->
        <script src="{{ URL::asset('assets/js/grayscale.min.js') }}"></script>

        <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
        <script>

          $(document).ready(function(){
            $(".imgblog img").addClass("img-fluid");

          });

          // var slideIndex = 0;
          // var clicked = 1;
          // showSlides(slideIndex);

          // function plusSlides(n) {
          //   // showSlides(slideIndex += n);
          //   if(n == -1){
          //     showSlides(slideIndex += n, -1, clicked);
          //   }else{
          //     showSlides(slideIndex += n, 1, clicked);
          //   }
          // }

          // function currentSlide(n) {
          //   showSlides(slideIndex = n);
          // }

          // function showSlides(n, min1 = 0, cliked = 0) {
          //   var i;
          //   var slides = document.getElementsByClassName("mySlides");
          //   var dots = document.getElementsByClassName("gmslide");
          //   if (n > slides.length) {slideIndex = 0}
          //     if (n < 0) {slideIndex = slides.length}
          //       for (i = 0; i < slides.length; i++) {
          //         slides[i].style.display = "none";
          //       }
          //       for (i = 0; i < dots.length; i++) {
          //         dots[i].className = dots[i].className.replace(" active", "");
          //       }
          //       if(cliked == 0){

          //           if(slideIndex >= slides.length){
          //             slideIndex = 1;
          //           }else{
          //             slideIndex++;
          //           }

          //           slides[slideIndex-1].style.display = "block";
          //           dots[slideIndex-1].className += " active";
          //           setTimeout(showSlides, 5000);

          //       }else{
          //         if(min1 == -1){
          //           if (n <= 0) {slideIndex = slides.length}
          //           slides[slideIndex-1].style.display = "block";
          //           dots[slideIndex-1].className += " active";
          //           //setTimeout(showSlides, 5000);

          //         }else{
          //           if(slideIndex >= slides.length){
          //             slideIndex = 1;
          //           }else{
          //             slideIndex++;
          //           }

          //           slides[slideIndex-1].style.display = "block";
          //           dots[slideIndex-1].className += " active";
          //           //setTimeout(showSlides, 5000);
          //         }
          //       }

          //       // slides[slideIndex-1].style.display = "block";
          //       // dots[slideIndex-1].className += " active";
          //       // setTimeout(showSlides, 2000);
          //     }
        </script>

        <script src="https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=xspx9ax4f39yi8pjfnpoe60sz9x5mz1xewzmxt918nsl47sh"></script>

        <script type="text/javascript">

              $(function(){
              $(".dropdown").hover(
                    function() {
                        $('.dropdown-menu', this).stop( true, true ).fadeIn("fast");
                        $(this).toggleClass('open');
                        $('b', this).toggleClass("caret caret-up");
                    },
                    function() {
                        $('.dropdown-menu', this).stop( true, true ).fadeOut("fast");
                        $(this).toggleClass('open');
                        $('b', this).toggleClass("caret caret-up");
                    });
            });

            tinymce.init({
              selector: '.mceEditor',
              entity_encoding : 'raw',
              mode : 'specific_textareas',
              //selector: 'textarea',
              //editor_selector : 'mceEditor',
              convert_urls: false,
              language : 'en',
              theme: 'modern',
              plugins: [
                'spellchecker,pagebreak,layer,table,save,insertdatetime,media,searchreplace,' +
                  'print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,' +
                  'nonbreaking,template,autoresize,' +
                  'anchor,charmap,hr,image,link,emoticons,code,textcolor,' +
                  'charmap,pagebreak'
              ],
              toolbar: 'insertfile undo redo| charmap | pagebreak | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image |  fontselect fontsizeselect | forecolor backcolor',
              pagebreak_separator: "<!-- my page break -->",
              image_advtab: true,
              autoresize_max_height: 350
            });
        </script>

          @yield('addingScriptJs')
    </body>
</html>
