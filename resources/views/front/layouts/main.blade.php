<!DOCTYPE html>
<html lang="en">
<head>

  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  <meta property="og:locale" content="nb_NO" />

  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="keywords"    content="">
  <meta name="description" content="">
  <meta name="author" content="">
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <link rel="shortcut icon" type="image/png" href="{{ URL::asset('assets/img/iconchange66.png') }}" />

  <title>@yield('title')</title>

  <!-- Bootstrap core CSS -->
  {{-- <link href="{{ URL::asset('assets/vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet"> --}}
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">


  <!-- Custom fonts for this template -->
  <link href="{{ URL::asset('assets/vendor/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic" rel="stylesheet" type="text/css">
  <link href='https://fonts.googleapis.com/css?family=Cabin:700' rel='stylesheet' type='text/css'>


  <!-- Custom styles for this template -->
  <link href="{{ URL::asset('assets/css/grayscale.min.css') }}" rel="stylesheet">
  <link href="{{ URL::asset('assets/css/slidequote.css') }}" rel="stylesheet">
  <link href="{{ URL::asset('assets/css/content.css') }}" rel="stylesheet">
  <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">

  @yield('addingStyle')
  <link href="{{ URL::asset('css/style-ari-front.css') }}" rel="stylesheet">

  <!-- <link href="css/bootstrap-4-hover-navbar.css" rel="stylesheet"> -->

    <script src='https://www.google.com/recaptcha/api.js'></script>
</head>
<a href="/internett1/index.php?t=publish.index&inline=edit" accesskey="E"></a>
{{-- <style type="text/css"> .container{ word-break: normal; }</style> --}}
<body id="page-top">

{{-- navbar menu new --}}
  <nav class="menuatas navbar navbar-default" style="background-color: #3a3838;border-color: #3a3838;">
    <div class="container">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar" style="background-color: white;"></span>
          <span class="icon-bar" style="background-color: white;"></span>
          <span class="icon-bar" style="background-color: white;"></span>
        </button>
         <a class="" href="/">
          <img src="{{ URL::asset('assets/img/logochange66.png') }}" style="height:53px; width:193px" alt="" />
        </a>
      </div>

      <!-- Collect the nav links, forms, and other content for toggling -->
      <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
         <ul class="nav navbar-nav" style="text-transform: uppercase;font-family: Cabin,'Helvetica Neue',Helvetica,Arial,sans-serif;"><!-- mx-auto -->
          @foreach( Menus::getNavbar(['NodeID' => Menus::getLangNode()]) as $menu )

            @if( count( Menus::getNavbar(['NodeID' => $menu->id]) ) > 0 )

              <li class="dropdown">
                @if($menu->id == 32)
                  <a href="{{ $menu->alias }}" class="nav-link js-scroll-trigger"> {{ $menu->title }}
                    <b class="caret"></b>
                  </a>
                @else
                  <a href="/{{ $menu->alias }}" class="nav-link js-scroll-trigger"> {{ $menu->title }}
                    <b class="caret"></b>
                  </a>
                @endif
                  <ul class="dropdown-menu" style="background-color: #909090;border: 0px solid rgba(0,0,0,.15);margin-top: 24px;">
                    @foreach( Menus::getNavbar(['NodeID' => $menu->id, 'whereNotIn' => [30]]) as $menusub )
                      <li><a href="/{{ $menusub->alias }}" class="nav-link">{{ $menusub->title }}</a></li>
                    @endforeach
                  </ul>
              </li>

            @else

               @if($menu->id == 52 || $menu->id == 4)
                
                 <li>
                  <a href="https://www.blogg.wisehouse.no/" class="nav-link js-scroll-trigger" target="_blank">{{ $menu->title }}</a>
                </li>

              @else
                 <li>
                  <a href="/{{ $menu->alias }}" class="nav-link js-scroll-trigger">{{ $menu->title }}</a>
                </li>

              @endif
             
            @endif

          @endforeach
          <li><a href="{{ route('registerfronts') }}" class="nav-link js-scroll-trigger">{{ Menus::getLanguageString('idRegister3') }}</a></li>
          
        </ul>
        <div class="my-2 my-lg-0" style="padding:12px;display: inline-block;">
          <a href="{{ route('changeLanguage', ['langID' => 'no']) }}">
            <img src="{{ URL::asset('assets/img/norwegia.png') }}" style="height:18px; width:23px;" alt="" />
          </a>
          <a href="{{ route('changeLanguage', ['langID' => 'en']) }}">
            <img src="{{ URL::asset('assets/img/flag-US.png') }}" style="height:18px; width:23px;" alt="" />
          </a>
         </div>
      </div><!-- /.navbar-collapse -->

    </div>
  </nav>
{{-- /navbar menu new --}}

  <!-- Navigation -->
  {{-- <nav class="menuatas navbar navbar-expand-lg navbar-light fixed-top @yield('addingNavBar')" id="mainNav">
    <div class="container-fluid col-md-9 garis-9">
      <a class="navbar-brand js-scroll-trigger" href="/">
        <img src="{{ URL::asset('assets/img/logowise.png') }}" style="height:53px; width:193px" alt="" />
      </a>
      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        Menu
        <i class="fa fa-bars"></i>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">

        <ul class="navbar-nav"><!-- mx-auto -->
          @foreach( Menus::getNavbar(['NodeID' => Menus::getLangNode()]) as $menu )

            @if( count( Menus::getNavbar(['NodeID' => $menu->id]) ) > 0 )

              <li class="dropdown">
                @if($menu->id == 32)
                  <a href="{{ $menu->alias }}" class="nav-link js-scroll-trigger"> {{ $menu->title }}
                    <b class="caret"></b>
                  </a>
                @else
                  <a href="/{{ $menu->alias }}" class="nav-link js-scroll-trigger"> {{ $menu->title }}
                    <b class="caret"></b>
                  </a>
                @endif
                  <ul class="dropdown-menu" style="background-color: #3180a9;border: 0px solid rgba(0,0,0,.15);">
                    @foreach( Menus::getNavbar(['NodeID' => $menu->id, 'whereNotIn' => [30]]) as $menusub )
                      <li><a href="/{{ $menusub->alias }}" class="nav-link">{{ $menusub->title }}</a></li>
                    @endforeach
                  </ul>
              </li>

            @else

              @if($menu->id == 52 || $menu->id == 4)
                
                 <li>
                  <a href="https://www.blogg.wisehouse.no/" class="nav-link js-scroll-trigger" target="_blank">{{ $menu->title }}</a>
                </li>

              @else
                 <li>
                  <a href="/{{ $menu->alias }}" class="nav-link js-scroll-trigger">{{ $menu->title }}</a>
                </li>

              @endif

            @endif

          @endforeach
          <li><a href="{{ route('registerfronts') }}" class="nav-link js-scroll-trigger">{{ Menus::getLanguageString('idRegister') }}</a></li>
          
        </ul>
        <div class="my-2 my-lg-0" style="padding:12px;display: inline-block;">
          <a href="{{ route('changeLanguage', ['langID' => 'no']) }}">
            <img src="{{ URL::asset('assets/img/norwegia.png') }}" style="height:18px; width:23px;" alt="" />
          </a>
          <a href="{{ route('changeLanguage', ['langID' => 'en']) }}">
            <img src="{{ URL::asset('assets/img/flag-US.png') }}" style="height:18px; width:23px;" alt="" />
          </a>
         </div>
       </div>
  </div>
</nav> --}}

<!-- Intro Header -->

@yield('content')



<section id="kontakt" class="content-section2 text-center hilang" style="background-color: #3a3838;">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 mx-auto">
        <h3><strong>{{ Menus::getLanguageString('idContactUs') }}</strong></h3>
      </div>
    </div>
  </div>
</section>

<section id="footer" class="content-section text-center masthead3 hilang">
        <div class="container">

          <div class="col-lg-12 mx-auto">
            <h2 class="major">{{ Menus::getLanguageString('idDYHAQuestions') }}</h2>
            <p>
              {{ Menus::getLanguageString('idContactText') }}
            </p>
            <div class="well well-sm" style="background-color: transparent;border: none;">


              <div class="row">
                <div class="col-lg-5">

                  <form action="{{ route('front.sendmailfront') }}" method="post">
                    {{ csrf_field() }}
                    <div class="form-group transparan">
                      <input type="text" class="form-control" name="first_name" value="" placeholder="{{ Menus::getLanguageString('idFirstName') }}">
                    </div>
                    <div class="form-group transparan">
                      <input type="text" class="form-control" name="last_name" value="" placeholder="{{ Menus::getLanguageString('idLastName') }}">
                    </div>
                    <div class="form-group transparan">
                      <input type="email" class="form-control" name="email" value="" placeholder="{{ Menus::getLanguageString('idEmail') }}">
                    </div>
                    <div class="form-group transparan">
                      <input type="tel" class="form-control" name="phone" value="" placeholder="{{ Menus::getLanguageString('idPhoneNumber') }}">
                    </div>
                    <div class="form-group transparan">
                      <textarea class="form-control" name="message" rows="3" placeholder="{{ Menus::getLanguageString('idMessage') }}"></textarea>
                    </div>

                    <div class="form-group transparan">
                      <div class="g-recaptcha" data-sitekey="{{env('CAPTCHA_KEY')}}"></div>
                      @if($errors->has('g-recaptcha-response'))
                        <span class="invalid-feedback" style="display: block;">
                          <strong>{{ $errors->first('g-recaptcha-response') }}</strong>
                        </span>
                      @endif  
                    </div>

                    <input type="hidden" name="contactus" value="1">
                    <input type="hidden" value="contact" name="from" />

                    <button class="btn btn-default" type="submit" name="action_publish_contactUs">
                      <i class="fa fa-paper-plane-o" aria-hidden="true"></i> {{ Menus::getLanguageString('idSubmit') }}
                    </button>
                  </form>
                </div>


                <div class="col-lg-7" style="text-align:left;line-height:2">

                  <ul class="contact">
                    <li class="fa-home">
                      WiseHouse AS<br />
                      Solbærvegen 5<br />
                      Elverum 2409                    </li>

                    <li class="fa-phone">+4793440771<br /></li>
                    <li class="fa-envelope"><a href="#">post@wisehouse.no</a></li>
                    <li class="fa-facebook"><a href="https://www.facebook.com/Wisehouse-Tools-382099292184354/">facebook.com</a></li>
                    <li class="fa-linkedin">
                      <a href="https://www.linkedin.com/company-beta/11119803">Linkedin.com</a>
                    </li>
                    <li class="fa-instagram"><a href="https://instagram.com/">instagram.com</a></li>
                    
                  </ul>
                </div>

              </div>
            </div>
          </div>

        </section>


        {{ $sukses = json_encode(\Session::get("success")) }}
        {{ $errors = json_encode(\Session::get('error')) }}
        <!-- Footer -->
        <footer style="background-color: #3a3838;">
          <div class="container text-center hilang">
            <p>Copyright &copy; WiseHouse 2018</p>
            @if (session()->get('LanguageID') == 'no')
              <p><a href="{{ url('/no/Personvernerklæring') }}">{{ Menus::getLanguageString('idTermsOfUse') }}</a></p>
              <ul class="list-unstyled">
                @foreach( Menus::getNavbar(['NodeID' => 48]) as $menusub )
                  <li><a href="/{{ $menusub->alias }}" class="nav-link">{{ $menusub->title }}</a></li>
                @endforeach
              </ul>
            @elseif (session()->get('LanguageID') == 'en')
              <p><a href="{{ url('en/Privacy_Statement') }}">{{ Menus::getLanguageString('idTermsOfUse') }}</a></p>
              <ul class="list-unstyled">
                @foreach( Menus::getNavbar(['NodeID' => 59]) as $menusub )
                  <li><a href="/{{ $menusub->alias }}" class="nav-link">{{ $menusub->title }}</a></li>
                @endforeach
              </ul>
            @endif

           
          </div>
        </footer>

        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-1857730-54"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'UA-1857730-54');
        </script>

        <!-- Bootstrap core JavaScript -->
        <script src="{{ URL::asset('assets/vendor/jquery/jquery.min.js') }}"></script>
        {{-- <script src="{{ URL::asset('assets/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script> --}}
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

        <!-- Plugin JavaScript -->
        <script src="{{ URL::asset('assets/vendor/jquery-easing/jquery.easing.min.js') }}"></script>

        <!-- Custom scripts for this template -->
        <script src="{{ URL::asset('assets/js/grayscale.min.js') }}"></script>

        <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
        <script>

          $(document).ready(function(){
            $(".imgblog img").addClass("img-fluid");

              var sukses = "{{ $sukses }}";
              var errors = "{{ $errors }}";
              if("{{ \Session::get('success') }}" != ""){
                alert("{{ \Session::get('success') }}");
              }

              if("{{\Session::get('error')}}"  != ""){
                //console.log(errors);
              }
          });

          
        </script>

        <script src="https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=xspx9ax4f39yi8pjfnpoe60sz9x5mz1xewzmxt918nsl47sh"></script>

        <script type="text/javascript">

              $(function(){
              $(".dropdown").hover(
                    function() {
                        $('.dropdown-menu', this).stop( true, true ).fadeIn("fast");
                        $(this).toggleClass('open');
                        $('b', this).toggleClass("caret caret-up");
                    },
                    function() {
                        $('.dropdown-menu', this).stop( true, true ).fadeOut("fast");
                        $(this).toggleClass('open');
                        $('b', this).toggleClass("caret caret-up");
                    });
            });

            tinymce.init({
              selector: '.mceEditor',
              entity_encoding : 'raw',
              mode : 'specific_textareas',
              //selector: 'textarea',
              //editor_selector : 'mceEditor',
              convert_urls: false,
              language : 'en',
              theme: 'modern',
              plugins: [
                'spellchecker,pagebreak,layer,table,save,insertdatetime,media,searchreplace,' +
                  'print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,' +
                  'nonbreaking,template,autoresize,' +
                  'anchor,charmap,hr,image,link,emoticons,code,textcolor,' +
                  'charmap,pagebreak'
              ],
              toolbar: 'insertfile undo redo| charmap | pagebreak | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image |  fontselect fontsizeselect | forecolor backcolor',
              pagebreak_separator: "<!-- my page break -->",
              image_advtab: true,
              autoresize_max_height: 350
            });
        </script>

          @yield('addingScriptJs')
    </body>
</html>
