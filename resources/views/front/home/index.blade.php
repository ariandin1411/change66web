@extends('front.layouts.main')

@section('title')
Home
@endsection

@section('addingScriptJs')

@if (\Session::has('success'))
<script>
  alert('{{ \Session::get("success") }}');
</script>
@endif
<script>
  $(document).ready(function(){
    $(".imgblog img").addClass("img-fluid");

    $('body').on('click','.refreshCaptcha',function(){

      // $('.loadCaptcha').addClass('fa-spin');

      // $.ajax({
      //   method:"get",
      //   url:"/api/refereshCaptcha",
      //   success: function(result){
      //     //console.log(result);
      //     $("#refereshrecapcha").attr("src",result);
      //     $('.loadCaptcha').removeClass('fa-spin');
      //   },
      //   error: function(error){
      //     alert("Try Again.");
      //   }
      // });

    });
  });

  var slideIndex = 0;
  var clicked = 1;
  showSlides(slideIndex);

  function plusSlides(n) {
    // showSlides(slideIndex += n);
    if(n == -1){
      showSlides(slideIndex += n, -1, clicked);
    }else{
      showSlides(slideIndex += n, 1, clicked);
    }
  }

  function currentSlide(n) {
    showSlides(slideIndex = n);
  }

  function showSlides(n, min1 = 0, cliked = 0) {
    var i;
    var slides = document.getElementsByClassName("mySlides");
    var dots = document.getElementsByClassName("gmslide");
    if (n > slides.length) {slideIndex = 0}
      if (n < 0) {slideIndex = slides.length}
        for (i = 0; i < slides.length; i++) {
          slides[i].style.display = "none";
        }
        for (i = 0; i < dots.length; i++) {
          dots[i].className = dots[i].className.replace(" active", "");
        }
        if(cliked == 0){

          if(slideIndex >= slides.length){
            slideIndex = 1;
          }else{
            slideIndex++;
          }

          slides[slideIndex-1].style.display = "block";
          dots[slideIndex-1].className += " active";
          setTimeout(showSlides, 5000);

        }else{
          if(min1 == -1){
            if (n <= 0) {slideIndex = slides.length}
              slides[slideIndex-1].style.display = "block";
            dots[slideIndex-1].className += " active";
            //setTimeout(showSlides, 5000);

          }else{
            if(slideIndex >= slides.length){
              slideIndex = 1;
            }else{
              slideIndex++;
            }

            slides[slideIndex-1].style.display = "block";
            dots[slideIndex-1].className += " active";
            //setTimeout(showSlides, 5000);
          }
        }

        // slides[slideIndex-1].style.display = "block";
        // dots[slideIndex-1].className += " active";
        // setTimeout(showSlides, 2000);
      }
    </script>
    @endsection

    @section('content')
    <div class="visible-lg besar" style="margin-top: -20px;">
      <header class="gambarutama2">
       <div class="intro-body">
         <!-- <div class="col-md-12 col-lg-12">  -->
           <div class="container">
             <div class="row">
               <div class="col-lg-6 bagianbawah" style="text-align: left;">
                 <h1 class="brand-heading regulasi1" style="margin-bottom: 0px;margin-left: -20px;">
                   <img src="{{ url('assets/img/logochange66.png') }}" class="img-responsive" alt="" />
                 </h1>
                 <br>
                 <br>
                 <h1 style="margin: 0 0 15px;"><b style="color: #901823;text-transform: none;">{{ Menus::getLanguageString('idC66H1') }}</b></h1>
                 <h2 style="margin: 0 0 15px;color: #901823;text-transform: none;">{{ Menus::getLanguageString('idC66H2') }}</h2>


                 {{-- <ul class="list-unstyled" style="text-align: left;">
                   <li><h3 class="" style="color: #000; margin: 0;">• {{ Menus::getLanguageString('idOrganization1') }}</h3></li><!-- #f7b100 -->
                   <li><h3 class="" style="color: #000; margin: 0;">• {{ Menus::getLanguageString('idManagementTeam') }}</h3></li>
                   <li><h3 class="" style="color: #000; margin: 0;">• {{ Menus::getLanguageString('idYourLeaderRole') }}</h3></li>
                 </ul> --}}
                 <br>
                 <br>
                 <br>
                 <br>
                 <h2 style="margin: 0 0 15px;color: #901823;">{{ Menus::getLanguageString('idC66H3') }}</h2> 



                 {{--  <a href="#vare_tjenester" class="btn btn-success js-scroll-trigger" style="background-color: #901823; border: 1px solid;"> {{ Menus::getLanguageString('idOurServices') }} </a> --}}

               </div>
               <div class="col-md-6">
               </div>


               {{-- <div class="col-lg-6 tempat ml-md-auto" style="text-align: left;">
                <div class="row" style="margin-top: 75px;">
                  <div class="col-md-12">
                    <div class="row" style="padding-left:40px;">
                      <div class="col-md-4">
                        <div class="center-text">
                         <div class="clip-wrap">

                          <div class="clip-each border-style-thin1">
                            <div class="overlay-content" style="color:black;">
                              {{ Menus::getLanguageString('idMotivate') }}
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>

                    <div class="col-md-4" style="padding-left:40px;">
                     <div class="center-text">
                      <div class="clip-wrap">
                        <div class="clip-each border-style-thin2">
                          <div class="overlay-content">{{ Menus::getLanguageString('idChange') }}</div>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="col-md-4" style="padding-left:65px">
                    <div class="center-text">
                      <div class="clip-wrap">
                        <div class="clip-each border-style-thin3">
                          <div class="overlay-content">{{ Menus::getLanguageString('idAction') }}</div>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div style="clear:both"></div>

                </div>
              </div>
            </div>
          </div> --}}
        </div>
      </div>
      <!-- </div>  -->

      <div class="panel panel-info" style="opacity: 0.9;border-color: #000000;margin-top: 5px;">
        <div class="panel-heading clearfix" style="background-color: #000000;border-color: #f0f8ff00;padding-top: 35px;padding-bottom: 35px;">
          <div class="container">
            
              <div class="form-group">
                
                <div class="row">

                  <div class="col-md-12">
                    <div class="seperator"></div>
                    <!-- <h1 class=" pull-left" style="color: white;">{{ Menus::getLanguageString('idC66S1') }}</h1>  -->
                    <div class="col-md-6" style="padding-left: 0px;">
                      <h3 class=" center" style="color: white;text-align: center;">{{ Menus::getLanguageString('idC66S1') }}</h3>                     
                    </div>
                    <div class="col-md-6">
                      <h3 class=" center" style="color: white;text-align: center;">{{ Menus::getLanguageString('idC66D1') }}</h3> 
                    </div>
                  </div>
                  
                  <div class="col-md-12">
                    <div class="seperator"></div>
                    <div class="col-md-6">
                      <p class="center" style="color: white;text-align: center;">{{ Menus::getLanguageString('idC66S2') }}</p>  
                    </div>
                    <div class="col-md-6">
                      <p class="center" style="color: white;text-align: center;"><a href="/registerfronts" style="color: white;">{{ Menus::getLanguageString('idFuApple') }}</a></p> 
                    </div>
                  </div>
                </div>
                
                <div class="row">
                  <div class="col-md-12">
                    <div class="seperator"></div>
                    <div class="col-lg-6 col-md-6">
                       <form action="{{ route('front.sendmailfront') }}" method="post">
                        {{ csrf_field() }}
                        <div class="input-group" style="width: 100%; ">
                          <div class="seperator"></div>
                          <input type="text" name="first_name" class="form-control" aria-label="..." placeholder="Skriv inn navn">
                        </div><!-- /input-group -->
                        <br>
                        <div class="input-group" style="width: 100%;">
                          <input type="text" class="form-control" name="email" aria-label="..." placeholder="skriv inn e-post">
                        </div><!-- /input-group -->
                        <br>
                        <div class="input-group" style="width: 100%;text-align: left;">
                          <span class="col-sm-3" style="padding-left: 0px;">
                            <img src="{{ captcha_src('flat') }}" class="img-responsive" width="100%">
                          </span>
                          <div class="col-sm-8" style="padding-left: 0px;">
                            <input type="text" class="form-control" name="captcha" aria-label="..." placeholder="Captcha">
                          </div>
                          <div class="col-sm-1" style="padding-left: 0px;">
                            <span class="btn btn-warning refreshCaptcha"> <i class="fa fa-refresh loadCaptcha"></i></span>
                          </div>
                        </div>
                        <br>
                        <div class="input-group" style="width: 100%;text-align: left;">

                          <input type="submit" name="sendemail" tabindex="1" value="{{ Menus::getLanguageString('idC66S3') }}" class="btn btn-danger" id="" style="font-size: 24px;"> 
                        </div>
                      </form>
                      <!-- /input-group -->
                    </div><!-- /.col-lg-6 -->
                    <div class="w-100"></div>
                    <div class="col-md-3">
                      <!-- <a href='https://itunes.apple.com/us/app/change66/id1290125333?ls=1&mt=8'><img src="{{ url('assets/img/apple.png') }}" class="img-responsive" alt="" /></a> -->
                      <a href='https://itunes.apple.com/us/app/change66/id1290125333?ls=1&mt=8'><img src="{{ url('assets/img/apple.png') }}" class="img-responsive" alt="" /></a> 
                    </div>
                    <div class="col-md-3">
                      <!-- <a href='https://play.google.com/store/apps/details?id=com.wisehouse.change66v2&hl=en_US'><img src="{{ url('assets/img/googleplay.png') }}" class="img-responsive" alt="" /></a> -->
                      <a href='https://play.google.com/store/apps/details?id=com.wisehouse.change66v2&hl=en_US'><img src="{{ url('assets/img/googleplay.png') }}" class="img-responsive" alt="" /></a>
                    </div>
                  </div>
                  
                </div>
            </div>
          </div>


          {{--  <div class="panel-body">
            <div class="btn-group pull-right">
              <a href="#" class="btn btn-default btn-sm">## Lock</a>
              <a href="#" class="btn btn-default btn-sm">## Delete</a>
              <a href="#" class="btn btn-default btn-sm">## Move</a>
            </div>
          </div> --}}
        </div>
      </div>

      <svg class="clip-svg">
        <defs>
          <clipPath id="octagon-clip" clipPathUnits="objectBoundingBox">
            <polygon points="0.3 0, 0.7 0, 1 0.0, 1 0.7, 0.7 1, 0.3 1, 0 0.7, 0 0.0" />
          </clipPath>
        </defs>
      </svg>
    </header>
    <div class="container-fluid" style="border-top: 10px solid white">
      &nbsp;
    </div>
  </div>


  <!-- untuk layar kecil -->

  <div class="hidden-lg">

   <div class="intro-body" style="background-image:url(https://change66.no/assets/img/picrune.png);background-repeat: round;margin-top: -20px;margin-bottom: -10px;">
     {{--  <img src="https://change66.no/assets/img/picrune.png" class="img-responsive"> --}}
     <!-- <div class="col-md-12 col-lg-12">  -->
       <div class="container">
         <div class="row">
           <div class="col-md-12 col-xs-10" style="text-align: left;">

             <h3 class="brand-heading regulasi1" style="margin-top: 25px; margin-bottom: 0px;margin-left: -15px;">
               <img src="{{ url('assets/img/logochange66.png') }}" class="img-responsive" alt="" style="max-width: 65%;" />
             </h3>
             <h4 style="margin: 0 0 15px;"><b style="color: #901823;text-transform: none;">{{ Menus::getLanguageString('idC66H1') }}</b></h4> 
             <h4 style="margin: 0 0 15px;color: #901823;text-transform: none;">{{ Menus::getLanguageString('idC66H2') }}</h4> 

             {{-- <ul class="list-unstyled" style="text-align: left;">
              <li><h3 class="" style="color: #000; margin: 0;">• {{ Menus::getLanguageString('idOrganization1') }}</h3></li><!-- #f7b100 -->
              <li><h3 class="" style="color: #000; margin: 0;">• {{ Menus::getLanguageString('idManagementTeam') }}</h3></li>
              <li><h3 class="" style="color: #000; margin: 0;">• {{ Menus::getLanguageString('idYourLeaderRole') }}</h3></li>
            </ul> --}}
            <br>
            <h4 style="margin: 0 0 15px;color: #901823;">{{ Menus::getLanguageString('idC66H3') }}</h4>


          </div>

          {{-- <div class="col-md-12 masthead3a tempat" style="text-align: center;">

            <div class="center-text" style="float:center">
             <div class="clip-wrap">
               <div class="clip-each border-style-thin1">
                <div class="overlay-content" style="color:black;">{{ Menus::getLanguageString('idMotivate') }}</div>
              </div>
            </div>
          </div>

          <div class="center-text"  style="float:center">
            <div class="clip-wrap">
              <div class="clip-each border-style-thin2">
                <div class="overlay-content">{{ Menus::getLanguageString('idChange') }}</div>
              </div>
            </div>
          </div>

          <div class="center-text" style="float:center">
            <div class="clip-wrap">
              <div class="clip-each border-style-thin3">
                <div class="overlay-content">{{ Menus::getLanguageString('idAction') }}</div>
              </div>
            </div>
          </div>
          <svg class="clip-svg">
            <defs>
              <clipPath id="octagon-clip" clipPathUnits="objectBoundingBox">
                <polygon points="0.3 0, 0.7 0, 1 0.0, 1 0.7, 0.7 1, 0.3 1, 0 0.7, 0 0.0" />
              </clipPath>
            </defs>
          </svg>
        </div> --}}
      </div>
    </div>
    {{-- <div class="container-fluid" style="border-top: 10px solid white">
      &nbsp;
    </div> --}}
  </div>

  <section id="download" class="text-center" style="background-color: #000000;margin-bottom: -10px;">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="panel-info" style="opacity: 0.9;border-color: #000000;margin-top: 5px;">
            <div class="clearfix" style="background-color: #000000;border-color: #f0f8ff00;padding-top: 35px;padding-bottom: 35px;">
              <div class="container">
                <div class="row">
                  <div class="col-md-12">
                    <h3 class="" style="color: white;">{{ Menus::getLanguageString('idC66S1') }}</h3> 
                  </div>

                  <div class="col-md-12">
                    <p class="" style="color: white;">{{ Menus::getLanguageString('idC66S2') }}</p> 
                  </div>
                </div>
                
                <div class="row">
                  <div class="col-lg-5">
                    <div class="input-group" style="width: 100%;">
                      <input type="text" class="form-control" aria-label="..." placeholder="Enter Name">
                    </div><!-- /input-group -->
                  </div><!-- /.col-lg-5 -->
                  <div class="col-lg-5">
                    <div class="input-group" style="width: 100%;">
                      <input type="text" class="form-control" aria-label="..." placeholder="Enter Email">
                    </div><!-- /input-group -->
                  </div><!-- /.col-lg-5 -->
                  <div class="col-lg-2" style="margin-top: -5px;">
                    <input type="submit" tabindex="1" value="{{ Menus::getLanguageString('idC66S3') }}" class="btn btn-danger" id="" style="font-size: 16px;"> 

                  </div><!-- /.col-lg-2 -->
                </div><!-- /.row -->
                <br>
                <br>
                <div class="row">
                  <hr style="height: 2px;background-color:white;">
                  <div class="col-md-12">
                    <h3 class="" style="color: white;text-align: center;">{{ Menus::getLanguageString('idC66D1') }}</h3>
                    <p class="" style="color: white;text-align: center;"><a href="/registerfronts" style="color: white;">{{ Menus::getLanguageString('idFuApple') }}</a></p>
                    <div class="col-md-12">
                     <a href='https://itunes.apple.com/us/app/change66/id1290125333?ls=1&mt=8'><img src="{{ url('assets/img/apple.png') }}" class="img-responsive" alt="" /></a>
                   </div>
                   <div class="col-md-12">
                    <a href='https://play.google.com/store/apps/details?id=com.wisehouse.change66v2&hl=en_US'><img src="{{ url('assets/img/googleplay.png') }}" class="img-responsive" alt="" /></a>
                  </div>
                </div>
              </div>

            </div>
            {{-- <div class="panel-body">
              <div class="btn-group pull-right">
                <a href="#" class="btn btn-default btn-sm">## Lock</a>
                <a href="#" class="btn btn-default btn-sm">## Delete</a>
                <a href="#" class="btn btn-default btn-sm">## Move</a>
              </div>
            </div> --}}
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

</div>


<a href="https://www.wisehouse.no/internett1/index.php?t=publish.indexbaru&amp;NodeID=&amp;inline=edit" accesskey="E"></a>

<!-- About Section -->

    <!-- <div class="container-fluid" style="border-top: 10px solid white">
        &nbsp;
      </div> -->
      <br>
      <br>
      <br>

      {{-- <section id="" class="content-section about regabout1"><!-- text-center -->
        <div class="container pengaturan1">
          <div class="row">
            <div class="col-lg-12 mx-auto">
             {!! $node->content1 !!}
           </div>
         </div>
       </div>
     </section> --}}

     <!-- Download Section -->

     <div class="container-fluid">
      <div class="row"> 
        <section class="download-section sectiondk tambahan2"></section>
        @foreach($blogchild as $b)
        @if($b->id <> 44)

        <section class="download-section sectiondk">
         <div class="container">
           <div class="row">
             <div class="col-md-4 col-xs-12 align-middle">
               <a class="imgblog">
                <img src="{{ $b->imagepath?Storage::url($b->imagepath):"" }}" title="" class="img-responsive" alt="Empatix mediabank Media ID 128" style="padding-bottom: 10px;" />


              </a>
            </div>

            <div class="col-md-8 col-xs-12">
             <h3 style=" margin: 0 0 5px;">
              <a href="{{ url($b->alias) }}" style="color:#000; ">{{ $b->title }}</a>
            </h3>
            {!! substr(strip_tags($b->content2), 0, 475) !!}...
            <div style="margin-top: 20px;padding-bottom: 40px;">
              <a href="{{ url($b->alias) }}"><button class="btn btn-default">Read More</button></a>
            </div>
          </div>

        </div>
      </div>
    </section>

    <!-- <div class="container-fluid" style="border-bottom: 2px solid black; margin-bottom:30px; margin-top:30px;"> -->
      <!-- <div class="row"  style="background: white;"> -->
        <div class="container">
          <!-- <div class="row"> -->
            <hr style="height: 2px;">
            <!-- </div> -->
          </div>
          <!-- </div> -->
          <!-- </div> -->
          @endif
          @endforeach

          {{-- 	<section id="vare_tjenester" class="download-section content-section" style="padding-top: 10px; padding-bottom: 0px;">
           <div class="container">
             <div class="row">
               <div class="col-md-4 align-middle">
                 <a class="imgblog">
                   <img src="{{ $icon2->getImages4?URL::asset($icon2->getImages4->path):"" }}" title="" alt="Empatix mediabank Media ID 127" />
                 </a>
               </div>

               <div class="col-md-8">
                 <h3 style=" margin: 0 0 5px;">
                  <a href="{{ url($icon2->alias) }}" style="color:#000;">{{ $icon2->title }}</a>
                </h3>
                {!! $icon2->content4 !!}
              </div>

            </div>
          </div>
        </section>

  <!-- <div class="container-fluid">
    <div class="row"  style="background: white;"> -->
      <div class="container">
        <!-- <div class="row"> -->
          <hr style="height: 2px;">
          <!-- </div> -->
        </div>
    <!-- </div>
    </div> -->

    <section>
      <div class="container">
        <div class="row">
          <div class="col-md-4 align-middle">
            <a class="imgblog">
              <img src="{{ $icon3->getImages4?URL::asset($icon3->getImages4->path):"" }}" title="" alt="Empatix mediabank Media ID 139" />
            </a>
          </div>

          <div class="col-md-8">
            <h3 style=" margin: 0 0 5px;">
              <a href="{{ url($icon3->alias) }}" style="color:#000; ">{{ $icon3->title }}</a>
            </h3>
            {!! $icon3->content4 !!}
          </div>

        </div>
      </div>
    </section>

	<!-- <div class="container-fluid">
    <div class="row"  style="background: white;"> -->
      <div class="container">
        <!-- <div class="row"> -->
          <hr style="height: 2px;">
          <!-- </div> -->
        </div>
    <!-- </div>
    </div> --}} -->

    {{--  <section id="vare_tjenester" class="download-section content-section" style="padding-top: 10px; padding-bottom: 30px;">
      <div class="container">
        <div class="row">

          <div class="col-md-2 align-middle">
            <a class="imgblog">
              <img src="{{ $icon4->getImages4?URL::asset($icon4->getImages4->path):"" }}" title="" alt="Empatix mediabank Media ID 138" />
            </a>
          </div>

          <div class="col-md-8">
            <h3 style=" margin: 0 0 5px;">
              <a href="{{ url($icon4->alias) }}" style="color:#000;">{{ $icon4->title }}</a>
            </h3>
            {!! $icon4->content4 !!}
          </div>

        </div>
      </div>
    </section> --}}

    <!-- Contact Section -->
    <section id="om_oss" class="content-section text-center regomos1" style="background-color: #3a3838;">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 mx-auto" style="color: white;">
           {!! $node_om_oss->content1 !!}
         </div>
       </div>
     </div>
   </section>

   <section id="hvor" class="content-section text-center  hvorfor">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 mx-auto">
          {!! $node->content2 !!}
        </div>
      </div>
    </div>
  </section>

  <!-- <section id="download" class="content-section regomos1" style="background-color: #000000;">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 mx-auto" style="color: white;">
          <div class="col-md-12">
              <h1 class=" pull-left" style="color: white;">{{ Menus::getLanguageString('idC66D1') }}</h1>
          </div>
        </div>
          <div class="col-md-2" style="margin-top: -5px;">
            <input type="submit" tabindex="1" value="Download Now" class="btn btn-danger" id="" style="font-size: 24px;">
          </div><!-- /.col-lg-2 -->
        </div>
      </div>
    </section> -->

  </div>
</div>
<!-- Map Section -->
{{--   <section id="carousel" class="content-section text-center masthead2">

  <div class="container">
    <div class="row">
      <div class="col-lg-12 mx-auto" style="color: black">

        <h2>{{ Menus::getLanguageString('idQuoteTitle') }}</h2>

        <div class="slideshow-container">

          <div style="position: relative;top: 15px;">
            <div style="display: block; width: 48%;float: left;text-align: right;">
              <a class="prev" onclick="plusSlides(-1)">❮</a>
            </div>

            <div style="display: block; width: 48%;float: right;text-align: left;">
              <a class="next" onclick="plusSlides(1)">❯</a>
            </div>
          </div>

          <div style="clear: both;"></div>

          @foreach($slideTitleChild as $stc)
          <div class="mySlides">
            {!! $stc->content1 !!}
            <p class="author">{{ $stc->title }}</p>
            <p class="author">
              <a href="/{{ $stc->alias }}" style="color:#000;">{{ Menus::getLanguageString('idReadMore') }}</a>
            </p>
            <center>
              <div class="gmslide" style="background:url({{ $stc->path  }});
              background-repeat : no-repeat;background-size : contain;">
              <div>
              </center>
            </div>
            @endforeach

          </div>

        </div>
      </div>
    </div>

  </section>
  --}}
  @endsection
